def get_bbox_coordinate(img):
    max_h = 0
    min_h = 512
    max_w = 0
    min_w = 512
    for h in range(len(img)):
        for w in range(len(img[0])):
            point_val = img[h, w]
            if point_val > 0:
                if max_h < h:
                    max_h = h
                if min_h > h:
                    min_h = h
                if max_w < w:
                    max_w = w
                if min_w > w:
                    min_w = w
    return max_h, max_w, min_h, min_w


def create_bounding_box(img, offset=10):
    bbox = [[0 for _ in range(len(img))] for _ in range(len(img[0]))]
    max_h, max_w, min_h, min_w = get_bbox_coordinate(img)
    for h in range(min_h - offset, max_h + offset):
        for w in range(min_w - offset, max_w + offset):
            bbox[h][w] = 1

    return bbox


def crop_bbox(img, max_h, max_w, min_h, min_w, offset=10):
    bbox = [[0 for _ in range(max_w - min_w + (offset * 2))] for _ in range(max_h - min_h + (offset * 2))]
    bbox_h = 0
    for h in range(min_h - offset, max_h + offset):
        bbox_w = 0
        for w in range(min_w - offset, max_w + offset):
            bbox[bbox_h][bbox_w] = img[h][w]
            bbox_w += 1
        bbox_h += 1
    return bbox
