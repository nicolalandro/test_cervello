import os

import h5py
import numpy as np
import pydicom

DATASET_DIR = '/media/mint/Caddy/TesiDataset/Cervello'
CASE = 'esempio1-20180820T174135Z-001/esempio1'


def read_dicom(dcm_name):
    dcm_path = os.path.join(DATASET_DIR, CASE, dcm_name)
    return pydicom.dcmread(dcm_path)


dir_path = os.path.join(DATASET_DIR, CASE)
files = os.listdir(dir_path)

radio_images = [f for f in files if f.startswith('MR')]

rs_files = [f for f in files if f.startswith('RS')]
matlab = [f for f in rs_files if f.endswith('.mat')][0]
matlab_path = os.path.join(DATASET_DIR, CASE, matlab)
data = h5py.File(matlab_path, 'r')
imgs = list(data.get('contours').get('Segmentation'))
x = []
y = []
for dcm_name in radio_images:
    ds = read_dicom(dcm_name)
    feature = ds.pixel_array
    label = imgs[ds.InstanceNumber - 1]
    for h in range(len(feature)):
        print(h)
        for w in range(len(feature[0])):
            print('\t', w)
            x.append([feature[h, w]])
            y.append(label[h, w])
x = np.array(x)
np.savetxt('../dataset/x.txt', x, fmt='%d')
y = np.array(y)
np.savetxt('../dataset/y.txt', y, fmt='%d')
