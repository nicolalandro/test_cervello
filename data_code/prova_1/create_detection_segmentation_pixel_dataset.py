import os
import re

import h5py
import numpy as np
import pydicom

from data_code.bbox_util import create_bounding_box

# DATASET_DIR = '/home/mint/Scrivania/Cervello'
DATASET_DIR = '/media/mint/Caddy/TesiDataset/Cervello'
CASE = 'esempio1-20180820T174135Z-001/esempio1'


# CASE = 'caso_005/T1'


def sorted_nicely(l):
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)


def read_dicom(dcm_name):
    dcm_path = os.path.join(DATASET_DIR, CASE, dcm_name)
    return pydicom.dcmread(dcm_path)


dir_path = os.path.join(DATASET_DIR, CASE)
files = os.listdir(dir_path)

radio_images = [f for f in files if f.startswith('MR')]
radio_images = sorted_nicely(radio_images)
rs_files = [f for f in files if f.startswith('RS')]
matlab = [f for f in rs_files if f.endswith('.mat')][0]
matlab_path = os.path.join(DATASET_DIR, CASE, matlab)

data = h5py.File(matlab_path, 'r')
imgs = list(data.get('contours').get('Segmentation'))

x = []
y = []
for dcm_name in radio_images:
    ds = read_dicom(dcm_name)
    img = imgs[ds.InstanceNumber - 1]
    if 1 in img:
        bbox = create_bounding_box(img)
        for h in range(len(bbox)):
            for w in range(len(bbox[0])):
                if bbox[h][w] == 1:
                    label = img[h][w]
                    feature = ds.pixel_array[h][w]
                    x.append(feature)
                    y.append(label)

x = np.array(x)
np.savetxt('../dataset/x_detection.txt', x, fmt='%d')
y = np.array(y)
np.savetxt('../dataset/y_detection.txt', y, fmt='%d')
