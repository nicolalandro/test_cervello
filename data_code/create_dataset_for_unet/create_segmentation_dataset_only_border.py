import os

import scipy.misc

from src.dicom_helper import get_3d_images_from_folder

DATASET_DIR = '/home/mint/Scrivania/Tesi/Cervello'
# DATASET_DIR = '/media/mint/Caddy/TesiDataset/Cervello'

OUTPUT_DIR_IMAGES = '../../dataset/tumor_segmentation_cut/images'
OUTPUT_DIR_LABELS = '../../dataset/tumor_segmentation_cut/labels'

CASES = os.listdir(DATASET_DIR)


def save_img(img, out, case, name):
    path = os.path.join(out, '%s_%d.jpg' % (case, name))
    scipy.misc.imsave(path, img)


for case in CASES:
    if case == 'caso_012' or case == 'caso_013' or case == 'caso_035':
        # this case has something different
        pass
    else:
        dir = os.path.join(DATASET_DIR, case, 'T1/')
        print('dir', dir)
        img3d, roi3d, _, _ = get_3d_images_from_folder(dir)
        for i in range(len(img3d[0][0])):
            roi = roi3d[:, :, i]
            if 255 in roi:
                save_img(img3d[:, :, i], OUTPUT_DIR_IMAGES, case, i)
                save_img(roi, OUTPUT_DIR_LABELS, case, i)
