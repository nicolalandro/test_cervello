import glob
import os

import pydicom
import scipy.misc

from src.dicom_helper import point_image_to_fill, coord2pixels

DATASET_DIR = '/home/mint/Scrivania/Tesi/Cervello'
# DATASET_DIR = '/media/mint/Caddy/TesiDataset/Cervello'

OUTPUT_DIR_IMAGES = '../../dataset/tumor_segmentation/images'
OUTPUT_DIR_LABELS = '../../dataset/tumor_segmentation/labels'

CASES = os.listdir(DATASET_DIR)


def save_images_from_folder(DIR):
    files = []
    dataset_re = DIR + '*.dcm'
    for fname in glob.glob(dataset_re, recursive=False):
        print("loading: {}".format(fname))
        files.append(pydicom.read_file(fname))

    print("file count: {}".format(len(files)))

    # skip files with no SliceLocation (eg scout views)
    roi_file = None
    slices = []
    skipcount = 0
    for f in files:
        if hasattr(f, 'SliceLocation'):
            slices.append(f)
        else:
            skipcount = skipcount + 1
            roi_file = f

    print("skipped, no SliceLocation: {}".format(skipcount))

    # ensure they are in the correct order
    slices = sorted(slices, key=lambda s: s.SliceLocation)

    # create 3D array
    img_shape = list(slices[0].pixel_array.shape)
    img_shape.append(len(slices))

    # ROI
    RTV = roi_file.ROIContourSequence[0]
    contours = [contour for contour in RTV.ContourSequence]
    img_contour_arrays = [coord2pixels(cdata, DIR, 'MR.') for cdata in contours]

    # fill 3D array with the images from the files
    for i, s in enumerate(slices):
        img_cotour_array = list(filter(lambda x: x[2] == s.SOPInstanceUID, img_contour_arrays))
        if len(img_cotour_array) != 0:
            img2d = s.pixel_array
            roi2d = None
            for _, single_img, _ in img_cotour_array:
                img = point_image_to_fill(single_img * 255)
                if roi2d is None:
                    roi2d = img
                else:
                    roi2d += img
            roi2d *= 255
            save_img(img2d, OUTPUT_DIR_IMAGES, case, i)
            save_img(roi2d, OUTPUT_DIR_LABELS, case, i)


def save_img(img, out, case, name):
    path = os.path.join(out, '%s_%d.jpg' % (case, name))
    scipy.misc.imsave(path, img)


for case in CASES:
    if case == 'caso_012' or case == 'caso_013' or case == 'caso_035':
        # this case has something different
        # AttributeError: 'Dataset' object has no attribute 'ContourSequence'
        pass
    else:
        dir = os.path.join(DATASET_DIR, case, 'T1/')
        print('dir', dir)
        save_images_from_folder(dir)
