import glob

import matplotlib.pyplot as plt
import numpy as np
import pydicom
from pymesh import base
import scipy.ndimage
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from skimage import measure

# DATASET_DIR = '/home/mint/Scrivania/Cervello/esempio1-20180820T174135Z-001/esempio1/*.dcm'
# DATASET_DIR = '/media/mint/Caddy/TesiDataset/Cervello/esempio1-20180820T174135Z-001/esempio1/*.dcm'
DATASET_DIR = '/home/mint/Scrivania/Tesi/Cervello/caso_001/T1/*.dcm'

# load the DICOM files
files = []
print('glob: {}'.format(DATASET_DIR))
for fname in glob.glob(DATASET_DIR, recursive=False):
    print("loading: {}".format(fname))
    files.append(pydicom.read_file(fname))

print("file count: {}".format(len(files)))

# skip files with no SliceLocation (eg scout views)
slices = []
skipcount = 0
for f in files:
    if hasattr(f, 'SliceLocation'):
        slices.append(f)
    else:
        skipcount = skipcount + 1

print("skipped, no SliceLocation: {}".format(skipcount))

# ensure they are in the correct order
slices = sorted(slices, key=lambda s: s.SliceLocation)

# pixel aspects, assuming all slices are the same
ps = slices[0].PixelSpacing
ss = slices[0].SliceThickness
ax_aspect = ps[1] / ps[0]
sag_aspect = ps[1] / ss
cor_aspect = ss / ps[0]

# create 3D array
img_shape = list(slices[0].pixel_array.shape)
img_shape.append(len(slices))
img3d = np.zeros(img_shape)

# fill 3D array with the images from the files
for i, s in enumerate(slices):
    img2d = s.pixel_array
    img3d[:, :, i] = img2d

print('img3d shape: ', img3d.shape)

# resampling
new_spacing = np.array([1, 1, 1])
ps = slices[0].PixelSpacing
ss = slices[0].SliceThickness
spacing = np.array([float(ss), float(ps[0]), float(ps[1])])
resize_factor = spacing / new_spacing
new_real_shape = img3d.shape * resize_factor
new_shape = np.round(new_real_shape)
real_resize_factor = new_shape / img3d.shape

new_spacing = spacing / real_resize_factor
pix_resampled = scipy.ndimage.interpolation.zoom(img3d, real_resize_factor)

print("Shape after resampling", pix_resampled.shape)

# create mash
threshold = 180
p = pix_resampled.transpose(2, 1, 0)
p = p[:, :, ::-1]
verts, faces, normals, values = measure.marching_cubes_lewiner(p, threshold)
faces = faces + 1

# Try to save into OFF file
with open('../3d_img/case1.off', 'w') as f:
    f.write('OFF\n')
    f.write('%d %d %d\n' % (len(verts), len(faces), 0))
    for v in verts:
        f.write('%d.0 %d.0 %d.0\n' % (v[0], v[1], v[2]))
    for fs in faces:
        f.write('%d %d %d\n' % (fs[0], fs[1], fs[2]))
# Try to save into OBJ file
with open('../3d_img/case1.obj', 'w') as thefile:
    for item in verts:
        thefile.write("v {0} {1} {2}\n".format(item[0], item[1], item[2]))

    for item in normals:
        thefile.write("vn {0} {1} {2}\n".format(item[0], item[1], item[2]))

    for item in faces:
        thefile.write("f {0}//{0} {1}//{1} {2}//{2}\n".format(item[0], item[1], item[2]))

# fig = plt.figure(figsize=(10, 10))
# ax = fig.add_subplot(111, projection='3d')
#
# # Fancy indexing: `verts[faces]` to generate a collection of triangles
# mesh = Poly3DCollection(verts[faces], alpha=0.1)
# face_color = [0.5, 0.5, 1]
# mesh.set_facecolor(face_color)
# ax.add_collection3d(mesh)

# ax.set_xlim(0, p.shape[0])
# ax.set_ylim(0, p.shape[1])
# ax.set_zlim(0, p.shape[2])
#
# plt.show()
