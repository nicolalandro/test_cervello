import os
import re

import h5py
import numpy as np
import pydicom
import scipy.misc
from scipy.misc import imsave

DATASET_DIR = '/media/mint/Caddy/TesiDataset/Cervello'
# DATASET_DIR = '/home/mint/Scrivania/Cervello'
# CASE = 'esempio1-20180820T174135Z-001/esempio1'
CASES = [
    'esempio1-20180820T174135Z-001/esempio1', 'esempio2-20180515T115718Z-001/esempio2',
    # 'esempio_003-20180515T115543Z-001/esempio_003/T1',
    # 'esempio_004-20180822T161723Z-001/esempio_004/T1',
]
for i in range(5, 9):
    CASES.append('caso_00%d/T1' % i)


# CASES = []
# for i in range(10, 30):
#     CASES.append('caso_0%d/T1' % i)


# CASE = 'caso_005/T1'


def sorted_nicely(l):
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)


def read_dicom(dcm_name):
    dcm_path = os.path.join(DATASET_DIR, CASE, dcm_name)
    return pydicom.dcmread(dcm_path)


for CASE in CASES:
    print('--- CASE %s ---' % CASE)
    dir_path = os.path.join(DATASET_DIR, CASE)
    files = os.listdir(dir_path)

    radio_images = [f for f in files if f.startswith('MR')]
    radio_images = sorted_nicely(radio_images)
    rs_files = [f for f in files if f.startswith('RS')]
    matlab = [f for f in rs_files if f.endswith('.mat')][0]
    matlab_path = os.path.join(DATASET_DIR, CASE, matlab)

    data = h5py.File(matlab_path, 'r')
    imgs = list(data.get('contours').get('Segmentation'))

    for dcm_name in radio_images:
        print('\t--- dcm_name %s ---' % dcm_name)
        ds = read_dicom(dcm_name)

        img = imgs[ds.InstanceNumber - 1]
        if 1 in img:
            img = [[255 if x == 0 else 0 for x in line] for line in img]
            scipy.misc.imsave('../dataset/cervello/images/%s.png' % dcm_name, np.array(ds.pixel_array))
            scipy.misc.imsave('../dataset/cervello/labels/%s.png' % dcm_name, np.array(img))
