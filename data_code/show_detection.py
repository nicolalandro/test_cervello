import os
import re

import h5py
import matplotlib.pyplot as plt
import pydicom

from data_code.bbox_util import create_bounding_box

# DATASET_DIR = '/home/mint/Scrivania/Cervello'
DATASET_DIR = '/media/mint/Caddy/TesiDataset/Cervello'
CASE = 'esempio1-20180820T174135Z-001/esempio1'


# CASE = 'caso_005/T1'


def sorted_nicely(l):
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)


def read_dicom(dcm_name):
    dcm_path = os.path.join(DATASET_DIR, CASE, dcm_name)
    return pydicom.dcmread(dcm_path)


dir_path = os.path.join(DATASET_DIR, CASE)
files = os.listdir(dir_path)

radio_images = [f for f in files if f.startswith('MR')]
radio_images = sorted_nicely(radio_images)
rs_files = [f for f in files if f.startswith('RS')]
matlab = [f for f in rs_files if f.endswith('.mat')][0]
matlab_path = os.path.join(DATASET_DIR, CASE, matlab)

data = h5py.File(matlab_path, 'r')
imgs = list(data.get('contours').get('Segmentation'))

for dcm_name in radio_images:
    ds = read_dicom(dcm_name)
    img = imgs[ds.InstanceNumber - 1]
    if 1 in img:
        bbox = create_bounding_box(img)
        f, axarr = plt.subplots(1, 3)
        axarr[1].imshow(ds.pixel_array, cmap=plt.cm.bone)
        axarr[1].imshow(bbox, cmap="Blues", alpha=0.5)
        axarr[0].imshow(img, cmap="Blues")
        axarr[2].imshow(ds.pixel_array, cmap=plt.cm.bone)
        plt.suptitle('Segmentation %s' % dcm_name)
        figManager = plt.get_current_fig_manager()
        figManager.full_screen_toggle()
        plt.show()
        exit()
