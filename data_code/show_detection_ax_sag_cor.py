import glob

import matplotlib.pyplot as plt
import numpy as np
import pydicom
from scipy.sparse import csc_matrix

from data_code.bbox_util import create_bounding_box


def coord2pixels(contour_dataset, path, prefix):
    """
    Given a contour dataset (a DICOM class) and path that has .dcm files of
    corresponding images. This function will return img_arr and contour_arr (2d image and contour pixels)
    Inputs
        contour_dataset: DICOM dataset class that is identified as (3006, 0016)  Contour Image Sequence
        path: string that tells the path of all DICOM images
    Return
        img_arr: 2d np.array of image with pixel intensities
        contour_arr: 2d np.array of contour with 0 and 1 labels
    """

    contour_coord = contour_dataset.ContourData
    # x, y, z coordinates of the contour in mm
    coord = []
    for i in range(0, len(contour_coord), 3):
        coord.append((contour_coord[i], contour_coord[i + 1], contour_coord[i + 2]))

    # extract the image id corresponding to given countour
    # read that dicom file
    img_ID = contour_dataset.ContourImageSequence[0].ReferencedSOPInstanceUID
    img = pydicom.read_file(path + prefix + img_ID + '.dcm')
    img_arr = img.pixel_array

    # physical distance between the center of each pixel
    x_spacing, y_spacing = float(img.PixelSpacing[0]), float(img.PixelSpacing[1])

    # this is the center of the upper left voxel
    origin_x, origin_y, _ = img.ImagePositionPatient

    # y, x is how it's mapped
    pixel_coords = [(np.ceil((y - origin_y) / y_spacing), np.ceil((x - origin_x) / x_spacing)) for x, y, _ in coord]

    # get contour data for the image
    rows = []
    cols = []
    for i, j in list(set(pixel_coords)):
        rows.append(i)
        cols.append(j)
    contour_arr = csc_matrix((np.ones_like(rows), (rows, cols)), dtype=np.int8,
                             shape=(img_arr.shape[0], img_arr.shape[1])).toarray()

    return img_arr, contour_arr, img_ID


DATASET_DIR = '/home/mint/Scrivania/Cervello/esempio1-20180820T174135Z-001/esempio1/*.dcm'
DIR = '/home/mint/Scrivania/Cervello/esempio1-20180820T174135Z-001/esempio1/'

# load the DICOM files
files = []
print('glob: {}'.format(DATASET_DIR))
for fname in glob.glob(DATASET_DIR, recursive=False):
    print("loading: {}".format(fname))
    files.append(pydicom.read_file(fname))

print("file count: {}".format(len(files)))

# skip files with no SliceLocation (eg scout views)
roi_file = None
slices = []
skipcount = 0
for f in files:
    if hasattr(f, 'SliceLocation'):
        slices.append(f)
    else:
        skipcount = skipcount + 1
        roi_file = f

print("skipped, no SliceLocation: {}".format(skipcount))

# ensure they are in the correct order
slices = sorted(slices, key=lambda s: s.SliceLocation)

# pixel aspects, assuming all slices are the same
ps = slices[0].PixelSpacing
ss = slices[0].SliceThickness
ax_aspect = ps[1] / ps[0]
sag_aspect = ps[1] / ss
cor_aspect = ss / ps[0]

# create 3D array
img_shape = list(slices[0].pixel_array.shape)
img_shape.append(len(slices))
img3d = np.zeros(img_shape)

# ROI
RTV = roi_file.ROIContourSequence[0]
contours = [contour for contour in RTV.ContourSequence]
img_contour_arrays = [coord2pixels(cdata, DIR, 'MR.') for cdata in contours]
roi3d = np.zeros(img_shape)

# fill 3D array with the images from the files
for i, s in enumerate(slices):
    img2d = s.pixel_array
    img_cotour_array = list(filter(lambda x: x[2] == s.SOPInstanceUID, img_contour_arrays))
    roi2d = roi3d[:, :, i]
    for _, val, _ in img_cotour_array:
        roi2d = roi2d + val
    roi2d *= 255
    roi3d[:, :, i] = roi2d
    img3d[:, :, i] = img2d

print('img3d shape: ', img3d.shape)
print('ROI image shape', roi3d.shape)

# plot 3 orthogonal slices
f, axarr = plt.subplots(2, 3)

i_transverse, i_sagittal, i_coronal = 115, img_shape[1] // 2 - 50, img_shape[0] // 2
print('image in position: ', i_transverse, i_sagittal, i_coronal)
# img
axarr[0, 0].imshow(img3d[:, :, i_transverse], cmap=plt.cm.bone)
axarr[0, 0].set_aspect(ax_aspect)
axarr[0, 0].set_title('transverse')

axarr[0, 1].imshow(img3d[:, i_sagittal, :], cmap=plt.cm.bone)
axarr[0, 1].set_aspect(sag_aspect)
axarr[0, 1].set_title('saggittal')

axarr[0, 2].imshow(img3d[i_coronal, :, :].T, cmap=plt.cm.bone)
axarr[0, 2].set_aspect(cor_aspect)
axarr[0, 2].set_title('coronal')

# roi
bbox = create_bounding_box(roi3d[:, :, i_transverse])
axarr[1, 0].imshow(bbox, cmap=plt.cm.bone)
axarr[1, 0].set_aspect(ax_aspect)
axarr[1, 0].set_title('transverse')

bbox1 = create_bounding_box(roi3d[:, i_sagittal, :].T)
axarr[1, 1].imshow(bbox1, cmap=plt.cm.bone)
axarr[1, 1].set_aspect(sag_aspect)
axarr[1, 1].set_title('saggittal')

bbox2 = create_bounding_box(roi3d[i_coronal, :, :])
axarr[1, 2].imshow(bbox2, cmap=plt.cm.bone)
axarr[1, 2].set_aspect(cor_aspect)
axarr[1, 2].set_title('coronal')

plt.show()
