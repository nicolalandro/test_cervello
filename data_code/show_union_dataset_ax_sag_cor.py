import matplotlib.pyplot as plt

from src.dicom_helper import get_3d_images_from_folder, point_image_to_fill

DIR = '/home/mint/Scrivania/Tesi/Cervello/caso_001/T1/'

img3d, roi3d, (ax_aspect, sag_aspect, cor_aspect), img_shape = get_3d_images_from_folder(DIR)

f, axarr = plt.subplots(1, 3)
i_transverse, i_sagittal, i_coronal = 115, img_shape[1] // 2, img_shape[0] // 2

# roi
axarr[0].imshow(roi3d[:, :, i_transverse], cmap=plt.cm.bone)
axarr[0].set_title('roi original')

# img
axarr[1].imshow(img3d[:, :, i_transverse], cmap=plt.cm.bone)
axarr[1].set_title('transverse')

# roi recostructed
roi = roi3d[:, :, i_transverse]

# roi
axarr[2].imshow(point_image_to_fill(roi), cmap=plt.cm.bone)
axarr[2].set_title('roi fill')


figManager = plt.get_current_fig_manager()
figManager.full_screen_toggle()
plt.show()
