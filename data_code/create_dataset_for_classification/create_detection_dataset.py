import os
from random import randint

import numpy as np
import scipy.misc

from data_code.bbox_util import get_bbox_coordinate, crop_bbox
from src.dicom_helper import get_3d_images_from_folder

DATASET_DIR = '/home/mint/Scrivania/Tesi/Cervello'
# DATASET_DIR = '/media/mint/Caddy/TesiDataset/Cervello'

OUTPUT_DIR_TUMOR = '../../dataset/tumor_classification/tumor'
OUTPUT_DIR_NOT_TUMOR = '../../dataset/tumor_classification/non_tumor'

CASES = os.listdir(DATASET_DIR)


def save_tumor_images(img3d, roi3d, out, name):
    # transverse
    transverse_counter = 0
    for i in range(len(img3d[0][0])):
        roi = roi3d[:, :, i] / 255
        if 1 in roi:
            transverse_counter += 1
            max_h, max_w, min_h, min_w = get_bbox_coordinate(roi)
            cropped_img = np.array(crop_bbox(img3d[:, :, i], max_h, max_w, min_h, min_w))
            tumor_path = os.path.join(out, '%s_%d.jpg' % (name, i))
            scipy.misc.imsave(tumor_path, cropped_img)
    print('transverse_count: ', transverse_counter)
    return transverse_counter


def save_non_tumor_images(counter, img3d, roi3d, out, name):
    # transverse
    for i in range(len(img3d[0][0])):
        if counter == 0:
            break
        roi = roi3d[:, :, i] / 255
        if 1 not in roi:
            min_h = randint(0, 125)
            max_h = randint(126, 154)
            min_w = randint(0, 125)
            max_w = randint(126, 154)
            cropped_img = np.array(crop_bbox(img3d[:, :, i], max_h, max_w, min_h, min_w))
            tumor_path = os.path.join(out, '%s_%d.jpg' % (name, i))
            scipy.misc.imsave(tumor_path, cropped_img)
            counter -= 1


for case in CASES:
    if case == 'caso_012':
        # this case has something different
        pass
    else:
        dir = os.path.join(DATASET_DIR, case, 'T1/')
        print('dir', dir)
        img3d, roi3d, _, _ = get_3d_images_from_folder(dir)
        counter = save_tumor_images(img3d, roi3d, OUTPUT_DIR_TUMOR, case)
        save_non_tumor_images(counter, img3d, roi3d, OUTPUT_DIR_NOT_TUMOR, case)
