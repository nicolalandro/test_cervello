import matplotlib.pyplot as plt

from src.dicom_helper import get_3d_images_from_folder

DIR = '/home/mint/Scrivania/Cervello/esempio1-20180820T174135Z-001/esempio1/'

# load the DICOM files

img3d, roi3d, (ax_aspect, sag_aspect, cor_aspect), img_shape = get_3d_images_from_folder(DIR)
# plot 3 orthogonal slices
f, axarr = plt.subplots(2, 3)

i_transverse, i_sagittal, i_coronal = 115, img_shape[1] // 2, img_shape[0] // 2
print('image in position: ', i_transverse, i_sagittal, i_coronal)
# img
axarr[0, 0].imshow(img3d[:, :, i_transverse], cmap=plt.cm.bone)
axarr[0, 0].set_aspect(ax_aspect)
axarr[0, 0].set_title('transverse')

axarr[0, 1].imshow(img3d[:, i_sagittal, :], cmap=plt.cm.bone)
axarr[0, 1].set_aspect(sag_aspect)
axarr[0, 1].set_title('saggittal')

axarr[0, 2].imshow(img3d[i_coronal, :, :].T, cmap=plt.cm.bone)
axarr[0, 2].set_aspect(cor_aspect)
axarr[0, 2].set_title('coronal')

# roi
axarr[1, 0].imshow(roi3d[:, :, i_transverse], cmap=plt.cm.bone)
axarr[1, 0].set_aspect(ax_aspect)
axarr[1, 0].set_title('transverse')

axarr[1, 1].imshow(roi3d[:, i_sagittal, :], cmap=plt.cm.bone)
axarr[1, 1].set_aspect(sag_aspect)
axarr[1, 1].set_title('saggittal')

axarr[1, 2].imshow(roi3d[i_coronal, :, :].T, cmap=plt.cm.bone)
axarr[1, 2].set_aspect(cor_aspect)
axarr[1, 2].set_title('coronal')

plt.show()
