import numpy as np
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.externals import joblib
from sklearn.model_selection import train_test_split

numb_x = np.loadtxt('../../dataset/x_detection.txt')
x = []
for n in numb_x:
    x.append([n])
y = np.loadtxt('../../dataset/y_detection.txt')

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.33)

clf = ExtraTreesClassifier()

print('fit...')
clf.fit(x_train, y_train)

print('save')
joblib.dump(clf, '../../model/extra_trees.pkl')
