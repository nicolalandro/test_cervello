from time import time

import numpy as np
import tensorflow as tf

from src.models.mlp import first_version

EPOCHS = 200

features_path = '../../dataset/x_detection.txt'
label_path = '../../dataset/y_detection.txt'
model_weight = "../../model/model_detection.h5"

x = np.loadtxt(features_path)
y_raw = np.loadtxt(label_path)
y = []
for val in y_raw:
    if val == 0:
        y.append(np.array([1, 0]))
    else:
        y.append(np.array([0, 1]))
y = np.array(y)

# x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.33)
x_train = x
y_train = y

model = first_version()
tensorboard = tf.keras.callbacks.TensorBoard(log_dir="../../logs/mlp_pix_based_{}".format(time()))

model.fit(x_train, y_train, epochs=EPOCHS, batch_size=1000, callbacks=[tensorboard])

model.save_weights(model_weight)
model_json = model.to_json()
with open("../model/model.json", "w") as json_file:
    json_file.write(model_json)
