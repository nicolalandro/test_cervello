from sklearn.externals import joblib
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt

from src.dataset_helper import get_radio_images_path, get_roi_images, get_image_and_instance_number

DATASET_DIR = '/media/mint/Caddy/TesiDataset/Cervello'
CASE = 'esempio1-20180820T174135Z-001/esempio1'
# CASE = 'caso_005/T1'


radio_images = get_radio_images_path(DATASET_DIR, CASE)
roi_images = get_roi_images(DATASET_DIR, CASE)
clf = joblib.load('../../model/extra_trees.pkl')

for dcm_name in radio_images:
    feature, instance_number = get_image_and_instance_number(DATASET_DIR, CASE, dcm_name)
    label = roi_images[instance_number]
    if 1 in label:
        image = [[0 for x in range(len(feature))] for y in range(len(feature[0]))]
        for h in range(len(feature)):
            print('--.', h, '---')
            x_feature = [[i] for i in feature[h]]
            x_label = [[i] for i in label[h]]
            prediction = clf.predict(x_feature)
            print("decision tree perecision: ", accuracy_score(x_label, prediction))
            # print("real:", label[h], "predicted:", prediction[0])
            image[h] = prediction
        f, axarr = plt.subplots(2, 3)
        axarr[0, 1].imshow(feature, cmap=plt.cm.bone)
        axarr[0, 1].imshow(label, cmap="Blues", alpha=0.5)
        axarr[0, 0].imshow(label, cmap="Blues")
        axarr[0, 2].imshow(feature, cmap=plt.cm.bone)
        axarr[1, 1].imshow(image, cmap="Blues")
        plt.suptitle('Segmentation %s' % dcm_name)
        figManager = plt.get_current_fig_manager()
        figManager.full_screen_toggle()
        plt.show()
        # exit()
