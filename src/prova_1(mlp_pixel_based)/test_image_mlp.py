import matplotlib.pyplot as plt
import tensorflow as tf
from sklearn.metrics import accuracy_score

from src.dataset_helper import get_radio_images_path, get_roi_images, get_image_and_instance_number
from src.models.mlp import first_version

DATASET_DIR = '/media/mint/Caddy/TesiDataset/Cervello'
CASE = 'esempio1-20180820T174135Z-001/esempio1'
model_weight_path = "../../model/model_detection.h5"

radio_images = get_radio_images_path(DATASET_DIR, CASE)
roi_images = get_roi_images(DATASET_DIR, CASE)
# model = tf.keras.models.model_from_json('../model/model.json')
model = first_version()
model.load_weights(model_weight_path)

for dcm_name in radio_images:
    feature, instance_number = get_image_and_instance_number(DATASET_DIR, CASE, dcm_name)
    label = roi_images[instance_number]
    if 1 in label:
        image = [[0 for x in range(len(feature))] for y in range(len(feature[0]))]
        for h in range(len(feature)):
            print('---', h, '---')
            prediction = model.predict(feature[h])
            # print('raw prediction', prediction)
            prediction = prediction.argmax(1)
            print("mlp perecision: ", accuracy_score(label[h], prediction))
            # print("real:", label[h], "predicted:", prediction)
            image[h] = prediction
        f, axarr = plt.subplots(2, 3)
        axarr[0, 1].imshow(feature, cmap=plt.cm.bone)
        axarr[0, 1].imshow(label, cmap="Blues", alpha=0.5)
        axarr[0, 0].imshow(label, cmap="Blues")
        axarr[0, 2].imshow(feature, cmap=plt.cm.bone)
        axarr[1, 1].imshow(image, cmap="Blues")
        plt.suptitle('Segmentation %s' % dcm_name)
        figManager = plt.get_current_fig_manager()
        figManager.full_screen_toggle()
        plt.show()
        exit()
