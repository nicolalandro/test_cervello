import glob
import math
from functools import cmp_to_key

import numpy as np
import pydicom
from PIL import Image, ImageDraw
from scipy.sparse import csc_matrix


def coord2pixels(contour_dataset, path, prefix):
    """
    Given a contour dataset (a DICOM class) and path that has .dcm files of
    corresponding images. This function will return img_arr and contour_arr (2d image and contour pixels)
    Inputs
        contour_dataset: DICOM dataset class that is identified as (3006, 0016)  Contour Image Sequence
        path: string that tells the path of all DICOM images
    Return
        img_arr: 2d np.array of image with pixel intensities
        contour_arr: 2d np.array of contour with 0 and 1 labels
    """

    contour_coord = contour_dataset.ContourData
    # x, y, z coordinates of the contour in mm
    coord = []
    for i in range(0, len(contour_coord), 3):
        coord.append((contour_coord[i], contour_coord[i + 1], contour_coord[i + 2]))

    # extract the image id corresponding to given countour
    # read that dicom file
    img_ID = contour_dataset.ContourImageSequence[0].ReferencedSOPInstanceUID
    img = pydicom.read_file(path + prefix + img_ID + '.dcm')
    img_arr = img.pixel_array

    # physical distance between the center of each pixel
    x_spacing, y_spacing = float(img.PixelSpacing[0]), float(img.PixelSpacing[1])

    # this is the center of the upper left voxel
    origin_x, origin_y, _ = img.ImagePositionPatient

    # y, x is how it's mapped
    pixel_coords = [(np.ceil((y - origin_y) / y_spacing), np.ceil((x - origin_x) / x_spacing)) for x, y, _ in coord]

    # get contour data for the image
    rows = []
    cols = []
    for i, j in list(set(pixel_coords)):
        rows.append(i)
        cols.append(j)
    contour_arr = csc_matrix((np.ones_like(rows), (rows, cols)), dtype=np.int8,
                             shape=(img_arr.shape[0], img_arr.shape[1])).toarray()

    return img_arr, contour_arr, img_ID


def get_3d_images_from_folder(DIR):
    files = []
    dataset_re = DIR + '*.dcm'
    for fname in glob.glob(dataset_re, recursive=False):
        print("loading: {}".format(fname))
        files.append(pydicom.read_file(fname))

    print("file count: {}".format(len(files)))

    # skip files with no SliceLocation (eg scout views)
    roi_file = None
    slices = []
    skipcount = 0
    for f in files:
        if hasattr(f, 'SliceLocation'):
            slices.append(f)
        else:
            skipcount = skipcount + 1
            roi_file = f

    print("skipped, no SliceLocation: {}".format(skipcount))

    # ensure they are in the correct order
    slices = sorted(slices, key=lambda s: s.SliceLocation)

    # pixel aspects, assuming all slices are the same
    ps = slices[0].PixelSpacing
    ss = slices[0].SliceThickness
    ax_aspect = ps[1] / ps[0]
    sag_aspect = ps[1] / ss
    cor_aspect = ss / ps[0]

    # create 3D array
    img_shape = list(slices[0].pixel_array.shape)
    img_shape.append(len(slices))
    img3d = np.zeros(img_shape)

    # ROI
    RTV = roi_file.ROIContourSequence[0]
    contours = [contour for contour in RTV.ContourSequence]
    img_contour_arrays = [coord2pixels(cdata, DIR, 'MR.') for cdata in contours]
    roi3d = np.zeros(img_shape)

    # fill 3D array with the images from the files
    for i, s in enumerate(slices):
        img2d = s.pixel_array
        img_cotour_array = list(filter(lambda x: x[2] == s.SOPInstanceUID, img_contour_arrays))
        roi2d = roi3d[:, :, i]
        for _, val, _ in img_cotour_array:
            roi2d = roi2d + val
        roi2d *= 255
        roi3d[:, :, i] = roi2d
        img3d[:, :, i] = img2d

    return img3d, roi3d, (ax_aspect, sag_aspect, cor_aspect), img_shape


def point_image_to_fill(img):
    coords = []
    height = len(img)
    width = len(img[0])
    for y in range(height):
        for x in range(width):
            if img[y, x] == 255:
                coords.append(np.array([x, y]))
    x = [c[0] for c in coords]
    x_center = np.mean(x)
    y = [c[1] for c in coords]
    y_center = np.mean(y)
    center = np.array([x_center, y_center])

    def p_gt(a, b):
        a_c = a - center
        t1 = math.atan2(a_c[0], a_c[1])
        b_c = b - center
        t2 = math.atan2(b_c[0], b_c[1])
        if t1 > t2:
            return 1
        else:
            return -1

    coords.sort(key=cmp_to_key(p_gt))
    print(coords)
    polygon = [(x[0], x[1]) for x in coords]
    out_img = Image.new('L', (width, height), 0)
    ImageDraw.Draw(out_img).polygon(polygon, outline=1, fill=1)
    return np.array(out_img)
