import h5py
import os

import numpy as np
import pydicom
import scipy.stats


def read_dicom(dataset_dir, case, dcm_name):
    dcm_path = os.path.join(dataset_dir, case, dcm_name)
    return pydicom.dcmread(dcm_path)


def get_radio_images_path(dataset_dir, case):
    dir_path = os.path.join(dataset_dir, case)
    files = os.listdir(dir_path)
    return [f for f in files if f.startswith('MR')]


def get_roi_images(dataset_dir, case):
    dir_path = os.path.join(dataset_dir, case)
    files = os.listdir(dir_path)
    rs_files = [f for f in files if f.startswith('RS')]
    matlab = [f for f in rs_files if f.endswith('.mat')][0]
    matlab_path = os.path.join(dataset_dir, case, matlab)
    data = h5py.File(matlab_path, 'r')
    imgs = list(data.get('contours').get('Segmentation'))
    return imgs


def get_image_and_instance_number(dataset_dir, case, dcm_name):
    ds = read_dicom(dataset_dir, case, dcm_name)
    feature = ds.pixel_array
    instance_number = ds.InstanceNumber - 1
    return feature, instance_number


def copute_pattern(pattern, pixel):
    array = np.array(pattern)
    mean = np.mean(array)
    variance = np.var(array)
    kurtosis = scipy.stats.kurtosis(array)
    skewness = scipy.stats.skew(array)
    entropy = scipy.stats.entropy(array)
    return list(array) + [pixel, mean, variance, kurtosis, skewness, entropy]


if __name__ == '__main__':
    pattern = [1, 2, 2, 2]
    pix = 30
    print(copute_pattern(pattern, pix))
