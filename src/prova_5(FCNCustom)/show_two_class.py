import os

import matplotlib.pyplot as plt
import numpy as np
import scipy.misc
from PIL import Image

from src.losses import keras_custom_losses
from src.metrics import custom_metrics
from src.models import cnn
import tensorflow.keras.backend as K

IMGS = '../../dataset/tumor_segmentation/images/'
LABELS = '../../dataset/tumor_segmentation/labels/'
MODEL_WEIGHT = '../../model/model_prova5.h5'
SELECT = 3


def to_img_array(array):
    return np.array([[v[0] for v in line] for line in array])


def main(select):
    images = []
    labels = []
    for image_name in os.listdir(IMGS)[:select]:
        image_path = os.path.join(IMGS, image_name)
        label_path = os.path.join(LABELS, image_name)

        image = scipy.misc.imread(image_path)
        image = np.array(Image.fromarray(image).resize((512, 512), Image.ANTIALIAS))
        image = np.array([[[v] for v in line] for line in image])
        images.append(image)

        label = scipy.misc.imread(label_path)
        label = np.array(Image.fromarray(label).resize((512, 512), Image.ANTIALIAS))
        label = np.array([[[1, 0] if v == 0 else [0, 1] for v in line] for line in label])
        labels.append(label)

    images = np.array(images)
    labels = np.array(labels, dtype=np.float32)

    model = cnn.v1_FCN_8_Custom((512, 512, 1), 2)
    model.load_weights(MODEL_WEIGHT)

    predictions = model.predict(images)
    # print(type(predictions), predictions)
    # with open('../models/pred.txt', 'wb') as f:
    #     np.save(f, np.array(predictions))
    # with open('../models/label.txt', 'wb') as f:
    #     np.save(f, labels)

    import tensorflow as tf
    with tf.Session() as sess:
        print('tp, tn, fp, fn', sess.run(custom_metrics.value_compute(labels, predictions)))
        print('jaccard', sess.run(custom_metrics.jacard(labels, predictions)))
        print('dice', sess.run(custom_metrics.dice(labels, predictions)))
        print('jaccard_keras_loss', sess.run(keras_custom_losses.jaccard_keras_loss(labels, predictions)))

    for img, label, prediction in zip(images, labels, predictions):
        # pred = []
        # for x in range(len(prediction)):
        #     line = []
        #     for y in range(len(prediction[0])):
        #         val = prediction[x][y]
        #         line.append(val.argmax(0) * 255)
        #     pred.append(line)
        with tf.Session() as sess:
            pred = K.argmax(prediction, 2).eval()
        im3 = np.array(pred)
        im1 = to_img_array(label)
        im2 = to_img_array(img)
        f, axarr = plt.subplots(1, 3)
        axarr[0].imshow(im1, cmap=plt.cm.bone)
        axarr[0].set_title('roi original')
        axarr[1].imshow(im2, cmap=plt.cm.bone)
        axarr[1].set_title('image')
        axarr[2].imshow(im3, cmap=plt.cm.bone)
        axarr[2].set_title('prediction')
        figManager = plt.get_current_fig_manager()
        figManager.full_screen_toggle()
        plt.show()


if __name__ == '__main__':
    main(SELECT)
