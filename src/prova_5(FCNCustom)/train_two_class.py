import os
from time import time

import numpy as np
import scipy.misc
import tensorflow as tf
from PIL import Image

from src.models import cnn

IMGS = '../../dataset/tumor_segmentation/images/'
LABELS = '../../dataset/tumor_segmentation/labels'
MODEL_WEIGHT = '../../model/model_prova5.h5'


def main():
    images = []
    labels = []
    for image_name in os.listdir(IMGS):
        image_path = os.path.join(IMGS, image_name)
        label_path = os.path.join(LABELS, image_name)

        image = scipy.misc.imread(image_path)
        image = np.array(Image.fromarray(image).resize((512, 512), Image.ANTIALIAS))
        image = np.array([[[v] for v in line] for line in image])
        images.append(image)

        label = scipy.misc.imread(label_path)
        label = np.array(Image.fromarray(label).resize((512, 512), Image.ANTIALIAS))
        label = np.array([[[1, 0] if v == 0 else [0, 1] for v in line] for line in label])
        labels.append(label)

    images = np.array(images)
    labels = np.array(labels)

    model = cnn.v1_FCN_8_Custom((512, 512, 1), 2)
    model.summary()
    tensorboard = tf.keras.callbacks.TensorBoard(log_dir="../../logs/%s_%s" % ('custom_fcn_prova5', time()))
    check_point = tf.keras.callbacks.ModelCheckpoint(MODEL_WEIGHT, monitor='val_loss', verbose=0, save_best_only=False,
                                                     save_weights_only=True, mode='auto', period=1)
    model.fit(x=images, y=labels, epochs=50, batch_size=5, callbacks=[tensorboard, check_point])


if __name__ == '__main__':
    main()
