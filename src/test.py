import numpy as np
from sklearn.externals import joblib
from sklearn.metrics import accuracy_score, jaccard_similarity_score, confusion_matrix, recall_score, precision_score, \
    f1_score

x_num = np.loadtxt('../dataset/x_detection.txt')
x_test = []
for n in x_num:
    x_test.append([n])
y_test = np.loadtxt('../dataset/y_detection.txt')

clf = joblib.load('../model/extra_trees.pkl')

predictions = clf.predict(x_test)
print("accuracy_score", accuracy_score(y_test, predictions))
print("precision_score", precision_score(y_test, predictions))
print("jaccard_similarity_score", jaccard_similarity_score(y_test, predictions))
print("recal macro", recall_score(y_test, predictions, average='macro'))
print("recal micro", recall_score(y_test, predictions, average='micro'))
print("recal weighted", recall_score(y_test, predictions, average='weighted'))
print("recal none", recall_score(y_test, predictions, average=None))
print("f1_score", f1_score(y_test, predictions))

print("confusion_matrix", confusion_matrix(y_test, predictions))
cnf_matrix = confusion_matrix(y_test, predictions)
normalized_cnf_matrix = cnf_matrix.astype('float') / cnf_matrix.sum(axis=1)[:, np.newaxis]
print("correlation_matrix", normalized_cnf_matrix)

FP = cnf_matrix.sum(axis=0) - np.diag(cnf_matrix)
print("false positives", FP)
FN = cnf_matrix.sum(axis=1) - np.diag(cnf_matrix)
print("false negatives", FN)
TP = np.diag(cnf_matrix)
print("true positives", TP)
TN = cnf_matrix.sum() - (FP + FN + TP)
print("true negatives", TN)
TPR = TP / (TP + FN)
print("sensitivity", TPR)
TNR = TN / (TN + FP)
print("specifity", TNR)
# Precision or positive predictive value
PPV = TP / (TP + FP)
print("positive predictive value", PPV)
# Negative predictive value
NPV = TN / (TN + FN)
print("negative predictive value", NPV)
# Fall out or false positive rate
FPR = FP / (FP + TN)
print("false positive rate", FPR)
# False negative rate
FNR = FN / (TP + FN)
print("false negative rate", FNR)
# False discovery rate
FDR = FP / (TP + FP)
print("false discovery rate", FDR)

# Overall accuracy
ACC = (TP + TN) / (TP + FP + FN + TN)
print("overall accuracy", ACC)
