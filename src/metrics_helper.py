from imblearn.metrics import geometric_mean_score

from src.metrics.two_class_metrics import TwoClassMetrics


def compute_all_metrics(y_true, y_pred):
    tcm = TwoClassMetrics(y_true, y_pred)
    return {
        'confusion_matrix': tcm.confusion_matrix(),
        'correlation_matrix': tcm.correlation_matrix(),
        'accuracy': tcm.accuracy(),
        'sensitivity': tcm.sensitivity(),
        'specifity': tcm.specifity(),
        'precision': tcm.precision(),
        'recall': tcm.recall(),
        'f_measure': tcm.f_measure(),
        'geometric mean': geometric_mean_score(y_true, y_pred),
        'jaccard': tcm.jaccard()
    }


if __name__ == '__main__':
    y_true = [1, 0, 1, 1, 0, 0]
    y_pred = [0, 0, 1, 1, 0, 1]
    metrics = compute_all_metrics(y_true, y_pred)
    for k, v in metrics.items():
        print(k, v)
