import numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K


def value_compute(y_true, y_pred):
    y_t = K.argmax(y_true, 3)
    y_p = K.argmax(y_pred, 3)
    y_true_f = K.flatten(y_t)
    y_pred_f = K.flatten(y_p)
    tp = K.sum(K.cast(y_true_f > 0, dtype=tf.int32) * K.cast(y_pred_f > 0, dtype=tf.int32))
    tn = K.sum(K.cast(y_true_f < 1, dtype=tf.int32) * K.cast(y_pred_f < 1, dtype=tf.int32))
    fn = K.sum(K.cast(y_true_f > 0, dtype=tf.int32) * K.cast(y_pred_f < 1, dtype=tf.int32))
    fp = K.sum(K.cast(y_true_f < 1, dtype=tf.int32) * K.cast(y_pred_f > 0, dtype=tf.int32))
    return tp, tn, fp, fn


def jacard(y_true, y_pred):
    tp, fp, tn, fn = value_compute(y_true, y_pred)
    return tp / (fn + tp + fp)


def dice(y_true, y_pred):
    # https://en.wikipedia.org/wiki/S%C3%B8rensen%E2%80%93Dice_coefficient
    tp, fp, tn, fn = value_compute(y_true, y_pred)
    return (2 * tp) / (2 * tp + fp + fn)


def precision(y_true, y_pred):
    tp, fp, tn, fn = value_compute(y_true, y_pred)
    return tp / (tp + fp)


def recall(y_true, y_pred):
    tp, fp, tn, fn = value_compute(y_true, y_pred)
    return tp / (tp + fn)


def sensitivity(y_true, y_pred):
    # is equal to recall
    tp, fp, tn, fn = value_compute(y_true, y_pred)
    return tp / (tp + fn)


def specifity(y_true, y_pred):
    tp, fp, tn, fn = value_compute(y_true, y_pred)
    return tn / (tn + fp)


def accuracy(y_true, y_pred):
    tp, fp, tn, fn = value_compute(y_true, y_pred)
    return (tp + tn) / (tp + tn + fp + fn)


def f_measure(y_true, y_pred):
    prec = precision(y_true, y_pred)
    rec = recall(y_true, y_pred)
    return 2 * (prec * rec / prec + rec)


def _manual_test():
    pred = [
        [
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [0, 1], [1, 0]]
        ],
        [
            [[0, 1], [1, 0], [1, 0]],
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [1, 0], [0, 1]]
        ],
    ]
    lab = [
        [
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [0, 1], [1, 0]]
        ],
        [
            [[0, 1], [1, 0], [0, 1]],
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [0, 1], [1, 0]]
        ],
    ]
    with tf.Session() as sess:
        pr = np.array(pred)
        la = np.array(lab)
        assert sess.run(value_compute(la, pr)) == (5, 10, 1, 2)
        print('tp, tn, fp, fn', sess.run(value_compute(la, pr)))
        print('jaccard', sess.run(jacard(la, pr)))


def _file_test():
    with open('pred.txt', 'rb') as f:
        predictions = np.load(f)
    with open('label.txt', 'rb') as f:
        labels = np.load(f)
    with tf.Session() as sess:
        print('tp, tn, fp, fn', sess.run(value_compute(labels, predictions)))
        print('jacard', sess.run(jacard(labels, predictions)))
        print('dice', sess.run(dice(labels, predictions)))
        print('precision', sess.run(precision(labels, predictions)))
        print('recall', sess.run(recall(labels, predictions)))
        print('sensitivity', sess.run(sensitivity(labels, predictions)))
        print('specifity', sess.run(specifity(labels, predictions)))
        print('accuracy', sess.run(accuracy(labels, predictions)))
        print('f_measure', sess.run(f_measure(labels, predictions)))


if __name__ == '__main__':
    _file_test()
    # _manual_test()
