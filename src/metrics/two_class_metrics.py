import numpy as np
from sklearn import metrics


class TwoClassMetrics:
    # https://en.wikipedia.org/wiki/Precision_and_recall

    def __init__(self, y_true, y_pred):
        self.y_pred = y_pred
        self.y_true = y_true
        self.cnf_matrix = None
        self.crr_matrix = None
        self.tp, self.fp, self.tn, self.fn = None, None, None, None

    def confusion_matrix(self):
        # y: True Label
        # x: Predicted Label
        if self.cnf_matrix is None:
            self.cnf_matrix = metrics.confusion_matrix(self.y_true, self.y_pred)
        return self.cnf_matrix

    def correlation_matrix(self):
        # y: True Label
        # x: Predicted Label
        cnf_matrix = self.confusion_matrix()
        self.crr_matrix = cnf_matrix.astype('float') / cnf_matrix.sum(axis=1)[:, np.newaxis]
        return self.crr_matrix

    def get_values(self):
        if self.tp is None:
            self.tn = self.confusion_matrix()[0][0]
            self.fn = self.confusion_matrix()[0][1]
            self.fp = self.confusion_matrix()[1][0]
            self.tp = self.confusion_matrix()[0][1]

        return self.tp, self.fp, self.tn, self.fn

    def precision(self):
        tp, fp, tn, fn = self.get_values()
        return tp / (tp + fp)

    def recall(self):
        # is equal to sensitivity
        tp, fp, tn, fn = self.get_values()
        return tp / (tp + fn)

    def sensitivity(self):
        # is equal to recall
        tp, fp, tn, fn = self.get_values()
        return tp / (tp + fn)

    def specifity(self):
        tp, fp, tn, fn = self.get_values()
        return tn / (tn + fp)

    def accuracy(self):
        tp, fp, tn, fn = self.get_values()
        return (tp + tn) / (tp + tn + fp + fn)

    def f_measure(self):
        precision = self.precision()
        recall = self.recall()
        return 2 * (precision * recall / precision + recall)

    def jaccard(self):
        # https://en.wikipedia.org/wiki/Jaccard_index
        tp, fp, tn, fn = self.get_values()
        return tp / (fn + tp + fp)


if __name__ == '__main__':
    y_true = [1, 2, 1, 1, 2, 2]
    y_pred = [2, 2, 1, 1, 2, 1]
    tcm = TwoClassMetrics(y_true, y_pred)

    print('confusion matrix\n', tcm.confusion_matrix())
    print('correlation matrix\n', tcm.correlation_matrix())

    tp, fp, tn, fn = tcm.get_values()
    print('true positives', tp)
    print('false positives', fp)
    print('true negatives', tn)
    print('false negatives', fn)

    print('precisison', tcm.sensitivity())
    print('recall', tcm.recall())
    print('sensitivity', tcm.sensitivity())
    print('specifity', tcm.specifity())
    print('accuracy', tcm.accuracy())
    print('f_measure', tcm.f_measure())
    print('jaccard', tcm.jaccard())
