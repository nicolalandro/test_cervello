import os

import numpy as np
import scipy.misc
import matplotlib.pyplot as plt

from src.models import cnn

IMGS = '../../dataset/cervello/images/'
LABELS = '../../dataset/cervello/labels'

image_path = os.path.join(IMGS, 'MR.1.3.12.2.1107.5.2.30.25970.201409181343441995534235.dcm.png')
label_path = os.path.join(LABELS, 'MR.1.3.12.2.1107.5.2.30.25970.201409181343441995534235.dcm.png')

image = scipy.misc.imread(image_path)
image = np.array([[[v] for v in line] for line in image])
label = scipy.misc.imread(label_path)
label = np.array([[[v] for v in line] for line in label])

model = cnn.FCN_8((512, 512, 1), 1)

model.fit(x=np.array([image]), y=np.array([label]), epochs=1, batch_size=10)

prediction = model.predict(x=np.array([image]))[0]
print(prediction.shape)
prediction = np.array([[v[0] for v in line] for line in prediction])

imgplot = plt.imshow(prediction, cmap=plt.cm.bone)
plt.show()
