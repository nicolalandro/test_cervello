import os

import matplotlib.pyplot as plt
import numpy as np
import scipy.misc

from src.models import cnn

IMGS = '../../dataset/cervello/images/'
LABELS = '../../dataset/cervello/labels/'
MODEL_WEIGHT = '../../model/model_prova4.h5'
SELECT = 3


def to_img_array(array):
    return np.array([[v[0] for v in line] for line in array])


def main(select):
    images = []
    labels = []
    for image_name in os.listdir(IMGS)[:select]:
        image_path = os.path.join(IMGS, image_name)
        label_path = os.path.join(LABELS, image_name)

        image = scipy.misc.imread(image_path)
        image = np.array([[[v] for v in line] for line in image])
        images.append(image)

        label = scipy.misc.imread(label_path)
        label = np.array([[[v / 255] for v in line] for line in label])
        labels.append(label)

    images = np.array(images)
    labels = np.array(labels)

    model = cnn.FCN_8((512, 512, 1), 1)
    model.load_weights(MODEL_WEIGHT)

    predictions = model.predict(images)

    for img, label, prediction in zip(images, labels, predictions):
        print(prediction)
        # pred = []
        # max = 0
        # min = 100000000000
        # for x in range(len(prediction)):
        #     line = []
        #     for y in range(len(prediction[0])):
        #         val = prediction[x][y]
        #         if val > max:
        #             max = val
        #         if val < min:
        #             min = val
        #         if val < 0:
        #             val = 1
        #         else:
        #             val = 0
        #         line.append(val)
        #     pred.append(line)
        # print(min, max)
        # im3 = np.array(pred)
        im1 = to_img_array(label)
        im2 = to_img_array(img)
        im3 = to_img_array(prediction)
        f, axarr = plt.subplots(1, 3)
        axarr[0].imshow(im1, cmap=plt.cm.bone)
        axarr[0].set_title('roi original')
        axarr[1].imshow(im2, cmap=plt.cm.bone)
        axarr[1].set_title('image')
        axarr[2].imshow(im3, cmap=plt.cm.bone)
        axarr[2].set_title('prediction')
        figManager = plt.get_current_fig_manager()
        figManager.full_screen_toggle()
        plt.show()


if __name__ == '__main__':
    main(SELECT)
