import os
from time import time

import numpy as np
import scipy.misc
import tensorflow as tf

from src.models import cnn

IMGS = '../../dataset/cervello/images/'
LABELS = '../../dataset/cervello/labels'
MODEL_WEIGHT = '../../model/model_prova4_2class.h5'


def main():
    images = []
    labels = []
    for image_name in os.listdir(IMGS):
        image_path = os.path.join(IMGS, image_name)
        label_path = os.path.join(LABELS, image_name)

        image = scipy.misc.imread(image_path)
        image = np.array([[[v] for v in line] for line in image])
        images.append(image)

        label = scipy.misc.imread(label_path)
        label = np.array([[[1, 0] if v == 0 else [0, 1] for v in line] for line in label])
        labels.append(label)

    images = np.array(images)
    labels = np.array(labels)

    model = cnn.FCN_8((512, 512, 1), 2)
    tensorboard = tf.keras.callbacks.TensorBoard(log_dir="../../logs/%s_%s" % ('fcn_prova4', time()))
    check_point = tf.keras.callbacks.ModelCheckpoint(MODEL_WEIGHT, monitor='val_loss', verbose=0, save_best_only=False,
                                                     save_weights_only=True, mode='auto', period=1)
    model.fit(x=images, y=labels, epochs=50, batch_size=5, callbacks=[tensorboard, check_point])


if __name__ == '__main__':
    main()
