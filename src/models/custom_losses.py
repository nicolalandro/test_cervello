import tensorflow.keras.backend as K


def two_dimension_jacard_coef(y_true, y_pred):
    y_t = K.argmax(y_true, 3)
    y_p = K.argmax(y_pred, 3)
    y_true_f = K.flatten(y_t)
    y_pred_f = K.flatten(y_p)
    intersection = K.sum(y_true_f * y_pred_f)
    return intersection / (K.sum(y_true_f) + K.sum(y_pred_f) - intersection)


def jacard_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (intersection + 1.0) / (K.sum(y_true_f) + K.sum(y_pred_f) - intersection + 1.0)


def jacard_coef_loss(y_true, y_pred):
    return -jacard_coef(y_true, y_pred)


def mod_jacard_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    # |(intersection + 0.01)/(all_1_prediction + 0.01) - 1|
    return K.abs((intersection + 0.01) / (K.sum(y_true_f) + K.sum(y_pred_f) - intersection + 0.01) - 1)


def mod_jacard_coef_loss(y_true, y_pred):
    return mod_jacard_coef(y_true, y_pred)
