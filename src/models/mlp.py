import tensorflow as tf


def first_version():
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(8, input_dim=1, activation='relu'))
    model.add(tf.keras.layers.Dense(2, activation='softmax'))
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    return model


def version_2(input_dim):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(32, input_dim=input_dim, activation='relu'))
    model.add(tf.keras.layers.Dense(16, activation='relu'))
    model.add(tf.keras.layers.Dense(2, activation='softmax'))
    model.compile(optimizer='adam', loss=tf.keras.losses.binary_crossentropy, metrics=['accuracy'])
    return model


def version_3(input_dim):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(64, input_dim=input_dim, activation='relu'))
    model.add(tf.keras.layers.Dense(32, input_dim=input_dim, activation='relu'))
    model.add(tf.keras.layers.Dense(16, activation='relu'))
    model.add(tf.keras.layers.Dense(2, activation='softmax'))
    model.compile(optimizer='adam', loss=tf.keras.losses.binary_crossentropy, metrics=['accuracy'])
    return model


def version_4(input_dim):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Flatten(input_shape=(input_dim,)))
    model.add(tf.keras.layers.Dense(512, activation='relu'))
    model.add(tf.keras.layers.Dropout(rate=0.2))
    model.add(tf.keras.layers.Dense(2, activation='softmax'))
    model.compile(optimizer='adam', loss=tf.keras.losses.binary_crossentropy, metrics=['accuracy'])
    return model


def version_5(input_dim):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Flatten(input_shape=(input_dim,)))
    model.add(tf.keras.layers.Dense(512, activation=tf.keras.activations.relu))
    model.add(tf.keras.layers.Dropout(rate=0.2))
    model.add(tf.keras.layers.Dense(512, activation=tf.keras.activations.elu))
    model.add(tf.keras.layers.Dense(2, activation='softmax'))
    model.compile(optimizer='adam', loss=tf.keras.losses.binary_crossentropy, metrics=['accuracy'])
    return model


def version_6(input_dim):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Flatten(input_shape=(input_dim,)))
    model.add(tf.keras.layers.Dense(64, activation=tf.keras.activations.relu))
    model.add(tf.keras.layers.Dense(32, activation=tf.keras.activations.sigmoid))
    model.add(tf.keras.layers.Dropout(rate=0.2))
    model.add(tf.keras.layers.Dense(32, activation=tf.keras.activations.sigmoid))
    model.add(tf.keras.layers.Dense(2, activation=tf.keras.activations.softmax))
    model.compile(optimizer=tf.keras.optimizers.Adam(lr=0.00001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False),
                  loss=tf.keras.losses.binary_crossentropy,
                  metrics=['accuracy'])
    return model
