import tensorflow as tf
from tensorflow.python.keras import Model, Sequential
from tensorflow.python.keras.layers import *
from tensorflow.python.keras.layers.core import *
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.layers.convolutional import Deconvolution2D

from src.losses import keras_custom_losses
from src.metrics import custom_metrics
from src.models.custom_losses import jacard_coef_loss, jacard_coef


def DnCNN():
    # https://github.com/husqin/DnCNN-keras
    inpt = Input(shape=(None, None, 1))
    # 1st layer, Conv+relu
    x = Conv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same')(inpt)
    x = Activation('relu')(x)
    # 15 layers, Conv+BN+relu
    for i in range(15):
        x = Conv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same')(x)
        x = BatchNormalization(axis=-1, epsilon=1e-3)(x)
        x = Activation('relu')(x)
        # last layer, Conv
    x = Conv2D(filters=1, kernel_size=(3, 3), strides=(1, 1), padding='same')(x)
    x = Subtract()([inpt, x])  # input - noise
    model = Model(inputs=inpt, outputs=x)
    model.compile(optimizer=Adam(), loss=['mse'])

    return model


def Convblock(channel_dimension, block_no, no_of_convs):
    Layers = []
    for i in range(no_of_convs):
        Conv_name = "conv" + str(block_no) + "_" + str(i + 1)

        # A constant kernel size of 3*3 is used for all convolutions
        Layers.append(
            Convolution2D(channel_dimension, kernel_size=(3, 3), padding="same", activation="relu", name=Conv_name))

    Max_pooling_name = "pool" + str(block_no)

    # Addding max pooling layer
    Layers.append(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name=Max_pooling_name))

    return Layers


def FCN_8_helper(image_shape):
    model = Sequential()
    model.add(Permute((1, 2, 3), input_shape=image_shape))

    for l in Convblock(64, 1, 2):
        model.add(l)

    for l in Convblock(128, 2, 2):
        model.add(l)

    for l in Convblock(256, 3, 3):
        model.add(l)

    for l in Convblock(512, 4, 3):
        model.add(l)

    for l in Convblock(512, 5, 3):
        model.add(l)

    model.add(Convolution2D(4096, kernel_size=(7, 7), padding="same", activation="relu", name="fc6"))

    # Replacing fully connnected layers of VGG Net using convolutions
    model.add(Convolution2D(4096, kernel_size=(1, 1), padding="same", activation="relu", name="fc7"))

    # Gives the classifications scores for each of the 21 classes including background
    model.add(Convolution2D(21, kernel_size=(1, 1), padding="same", activation="relu", name="score_fr"))

    Conv_size = model.layers[-1].output_shape[2]  # 16 if image size if 512
    # print(Conv_size)

    model.add(Deconvolution2D(21, kernel_size=(4, 4), strides=(2, 2), padding="valid", activation=None, name="score2"))

    # O = ((I-K+2*P)/Stride)+1
    # O = Output dimesnion after convolution
    # I = Input dimnesion
    # K = kernel Size
    # P = Padding

    # I = (O-1)*Stride + K
    Deconv_size = model.layers[-1].output_shape[2]  # 34 if image size is 512*512

    # print(Deconv_size)
    # 2 if image size is 512*512
    Extra = (Deconv_size - 2 * Conv_size)

    # print(Extra)

    # Cropping to get correct size
    model.add(Cropping2D(cropping=((0, Extra), (0, Extra))))

    return model


def FCN_8(image_shape, classes):
    fcn_8 = FCN_8_helper(image_shape)
    # Calculating conv size after the sequential block
    # 32 if image size is 512*512
    Conv_size = fcn_8.layers[-1].output_shape[2]

    # Conv to be applied on Pool4
    skip_con1 = Convolution2D(21, kernel_size=(1, 1), padding="same", activation=None, name="score_pool4")

    # Addig skip connection which takes adds the output of Max pooling layer 4 to current layer
    Summed = add(inputs=[skip_con1(fcn_8.layers[14].output), fcn_8.layers[-1].output])

    # Upsampling output of first skip connection
    x = Deconvolution2D(21, kernel_size=(4, 4), strides=(2, 2), padding="valid", activation=None, name="score4")(Summed)
    x = Cropping2D(cropping=((0, 2), (0, 2)))(x)

    # Conv to be applied to pool3
    skip_con2 = Convolution2D(21, kernel_size=(1, 1), padding="same", activation=None, name="score_pool3")

    # Adding skip connection which takes output og Max pooling layer 3 to current layer
    Summed = add(inputs=[skip_con2(fcn_8.layers[10].output), x])

    # Final Up convolution which restores the original image size
    Up = Deconvolution2D(classes, kernel_size=(16, 16), strides=(8, 8),
                         padding="valid", activation=None, name="upsample")(Summed)

    # Cropping the extra part obtained due to transpose convolution
    final = Cropping2D(cropping=((0, 8), (0, 8)))(Up)

    model = Model(fcn_8.input, final)
    model.compile(optimizer='rmsprop', loss=jacard_coef_loss,
                  metrics=[tf.keras.metrics.binary_accuracy, tf.keras.metrics.mean_squared_error, jacard_coef])
    return model


def FCN_16_helper(image_shape):
    model = Sequential()
    model.add(Permute((1, 2, 3), input_shape=image_shape))

    for l in Convblock(64, 1, 2):
        model.add(l)

    for l in Convblock(128, 2, 2):
        model.add(l)

    for l in Convblock(256, 3, 3):
        model.add(l)

    for l in Convblock(512, 4, 3):
        model.add(l)

    for l in Convblock(512, 5, 3):
        model.add(l)

    model.add(Convolution2D(4096, kernel_size=(7, 7), padding="same", activation="relu", name="fc_6"))

    # Replacing fully connnected layers of VGG Net using convolutions
    model.add(Convolution2D(4096, kernel_size=(1, 1), padding="same", activation="relu", name="fc7"))

    # Gives the classifications scores for each of the 21 classes including background
    model.add(Convolution2D(21, kernel_size=(1, 1), padding="same", activation="relu", name="score_fr"))

    Conv_size = model.layers[-1].output_shape[2]  # 16 if image size if 512
    print(Conv_size)

    model.add(Deconvolution2D(21, kernel_size=(4, 4), strides=(2, 2), padding="valid", activation=None, name="score2"))

    # O = ((I-K+2*P)/Stride)+1
    # O = Output dimesnion after convolution
    # I = Input dimnesion
    # K = kernel Size
    # P = Padding

    # I = (O-1)*Stride + K
    Deconv_size = model.layers[-1].output_shape[2]  # 34 if image size is 512*512

    print(Deconv_size)
    # 2 if image size is 512*512
    Extra = (Deconv_size - 2 * Conv_size)

    print(Extra)

    # Cropping to get correct size
    model.add(Cropping2D(cropping=((0, Extra), (0, Extra))))
    return model


def FCN_16(image_shape, classes):
    fcn_16 = FCN_16_helper(image_shape)

    # Calculating conv size after the sequential block
    # 32 if image size is 512*512
    Conv_size = fcn_16.layers[-1].output_shape[2]

    skip_con = Convolution2D(21, kernel_size=(1, 1), padding="same", activation=None, name="score_pool4")

    # Addig skip connection which takes adds the output of Max pooling layer 4 to current layer
    Summed = add(inputs=[skip_con(fcn_16.layers[14].output), fcn_16.layers[-1].output])

    Up = Deconvolution2D(classes, kernel_size=(32, 32), strides=(16, 16), padding="valid", activation=None,
                         name="upsample_new")

    # 528 if image size is 512*512
    Deconv_size = (Conv_size - 1) * 16 + 32

    # 16 if image size is 512*512
    extra_margin = (Deconv_size - Conv_size * 16)

    # Cropping to get the original size of the image
    crop = Cropping2D(cropping=((0, extra_margin), (0, extra_margin)))
    model = Model(fcn_16.input, crop(Up(Summed)))
    model.compile(optimizer='rmsprop', loss=jacard_coef_loss,
                  metrics=[tf.keras.metrics.binary_accuracy, tf.keras.metrics.mean_squared_error, jacard_coef])
    return model


def SegNet(input_shape=(360, 480, 3), classes=12):
    img_input = Input(shape=input_shape)
    x = img_input
    # Encoder
    x = Conv2D(64, (3, 3), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)

    x = Conv2D(128, (3, 3), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)

    x = Conv2D(256, (3, 3), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)

    x = Conv2D(512, (3, 3), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)

    # Decoder
    x = Conv2D(512, (3, 3), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)

    x = UpSampling2D(size=(2, 2))(x)
    x = Conv2D(256, (3, 3), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)

    x = UpSampling2D(size=(2, 2))(x)
    x = Conv2D(128, (3, 3), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)

    x = UpSampling2D(size=(2, 2))(x)
    x = Conv2D(64, (3, 3), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)

    x = Conv2D(classes, (1, 1), padding="valid")(x)
    x = Reshape((input_shape[0] * input_shape[1], classes))(x)
    x = Activation("softmax")(x)
    model = Model(img_input, x)
    model.compile(optimizer='rmsprop', loss=tf.keras.losses.mean_squared_error)
    model.summary()
    return model


def v1_down_and_up_sampling(img_w=256, img_h=256, n_labels=2, kernel=3):
    # https://github.com/imlab-uiip/keras-segnet/blob/master/build_model.py
    encoding_layers = [
        Conv2D(64, kernel, input_shape=(img_h, img_w, 1)),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(64, kernel),
        BatchNormalization(),
        Activation('relu'),
        MaxPooling2D(),

        Conv2D(128, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(128, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        MaxPooling2D(),

        Conv2D(256, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(256, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(256, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        MaxPooling2D(),

        Conv2D(512, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(512, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(512, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        MaxPooling2D(),

        Conv2D(512, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(512, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(512, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        MaxPooling2D(),
    ]

    decoding_layers = [
        UpSampling2D(),
        Conv2D(512, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(512, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(512, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),

        UpSampling2D(),
        Conv2D(512, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(512, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(256, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),

        UpSampling2D(),
        Conv2D(256, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(256, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(128, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),

        UpSampling2D(),
        Conv2D(128, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(64, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),

        UpSampling2D(),
        Conv2D(64, kernel, kernel),
        BatchNormalization(),
        Activation('relu'),
        Conv2D(n_labels, 1, 1),
        BatchNormalization(),
    ]

    model = tf.keras.models.Sequential()

    for l in encoding_layers:
        model.add(l)
    for l in decoding_layers:
        model.add(l)

    model.compile(optimizer='rmsprop', loss=tf.keras.losses.binary_crossentropy)
    model.summary()
    return model


def v1_FCN_8_Custom(image_shape, classes):
    model = Sequential()
    model.add(Permute((1, 2, 3), input_shape=image_shape))

    for l in Convblock(64, 1, 2):
        model.add(l)

    for l in Convblock(128, 2, 2):
        model.add(l)

    for l in Convblock(256, 3, 3):
        model.add(l)

    for l in Convblock(512, 4, 3):
        model.add(l)

    for l in Convblock(512, 5, 3):
        model.add(l)

    model.add(Convolution2D(4096, kernel_size=(7, 7), padding="same", activation="relu", name="fc6"))

    # Replacing fully connnected layers of VGG Net using convolutions
    model.add(Convolution2D(4096, kernel_size=(1, 1), padding="same", activation="relu", name="fc7"))

    # Gives the classifications scores for each of the 21 classes including background
    model.add(Convolution2D(21, kernel_size=(1, 1), padding="same", activation="relu", name="score_fr"))

    Conv_size = model.layers[-1].output_shape[2]  # 16 if image size if 512
    # print(Conv_size)

    model.add(Deconvolution2D(21, kernel_size=(4, 4), strides=(2, 2), padding="valid", activation=None, name="score2"))

    # O = ((I-K+2*P)/Stride)+1
    # O = Output dimesnion after convolution
    # I = Input dimnesion
    # K = kernel Size
    # P = Padding

    # I = (O-1)*Stride + K
    Deconv_size = model.layers[-1].output_shape[2]  # 34 if image size is 512*512
    # 2 if image size is 512*512
    Extra = (Deconv_size - 2 * Conv_size)
    # Cropping to get correct size
    model.add(Cropping2D(cropping=((0, Extra), (0, Extra))))

    skip_con1 = Convolution2D(21, kernel_size=(1, 1), padding="same", activation=None, name="score_pool4")

    # Addig skip connection which takes adds the output of Max pooling layer 4 to current layer
    Summed = add(inputs=[skip_con1(model.layers[14].output), model.layers[-1].output])

    # Upsampling output of first skip connection
    x = Deconvolution2D(21, kernel_size=(4, 4), strides=(2, 2), padding="valid", activation=None, name="score4")(Summed)
    x = Cropping2D(cropping=((0, 2), (0, 2)))(x)

    # Conv to be applied to pool3
    skip_con2 = Convolution2D(21, kernel_size=(1, 1), padding="same", activation=None, name="score_pool3")

    # Adding skip connection which takes output og Max pooling layer 3 to current layer
    Summed = add(inputs=[skip_con2(model.layers[10].output), x])

    # Final Up convolution which restores the original image size
    Up = Deconvolution2D(21, kernel_size=(16, 16), strides=(8, 8),
                         padding="valid", activation=None, name="upsample")(Summed)

    # Cropping the extra part obtained due to transpose convolution
    final = Cropping2D(cropping=((0, 8), (0, 8)))(Up)
    last = Dense(classes, activation=tf.keras.activations.softmax)(final)

    fcn = Model(model.input, last)
    fcn.compile(optimizer='rmsprop', loss=keras_custom_losses.jaccard_keras_loss,
                metrics=[custom_metrics.accuracy, custom_metrics.jacard, custom_metrics.dice,
                         custom_metrics.precision, custom_metrics.recall,
                         custom_metrics.specifity, custom_metrics.sensitivity,
                         custom_metrics.f_measure])
    return fcn
