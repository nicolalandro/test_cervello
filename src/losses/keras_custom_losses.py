import tensorflow.keras.backend as K
import tensorflow as tf
import numpy as np


def jaccard_keras_loss(y_true, y_pred):
    # K argmax non ha gradiente quindi non posso farci una loss
    # y_t = K.argmax(y_true, 3)
    # y_p = K.argmax(y_pred, 3)
    # y_true_f = K.flatten(y_t)
    # y_pred_f = K.flatten(y_p)
    # intersection = K.cast(K.sum(y_true_f * y_pred_f), dtype=tf.float32)
    # y_true_count = K.cast(K.sum(y_true_f), dtype=tf.float32)
    # y_pred_count = K.cast(K.sum(y_pred_f), dtype=tf.float32)
    # var_01 = K.constant(0.01, dtype=tf.float32)
    # var_1 = K.constant(1, dtype=tf.float32)
    # return var_1 - (intersection + var_01) / (y_true_count + y_pred_count - intersection + var_01)
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return 1 - ((intersection + 0.01) / (K.sum(y_true_f) + K.sum(y_pred_f) - intersection + 0.01))


def _manual_test():
    pred = [
        [
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [0, 1], [1, 0]]
        ],
        [
            [[0, 1], [1, 0], [1, 0]],
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [1, 0], [0, 1]]
        ],
    ]
    lab = [
        [
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [0, 1], [1, 0]]
        ],
        [
            [[0, 1], [1, 0], [0, 1]],
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [0, 1], [1, 0]]
        ],
    ]
    with tf.Session() as sess:
        pr = np.array(pred)
        la = np.array(lab)
        print('jaccard loss', sess.run(jaccard_keras_loss(la, pr)))


if __name__ == '__main__':
    _manual_test()
