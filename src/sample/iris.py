from sklearn.datasets import load_iris
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
import tensorflow as tf

iris = load_iris()
examples = iris.data
truths = iris.target

x_train, x_test, y_train, y_test = train_test_split(examples, truths, test_size=0.33)

model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Dense(units=8, activation='relu', input_dim=4))
model.add(tf.keras.layers.Dense(units=16, activation='relu', input_dim=4))
model.add(tf.keras.layers.Dense(units=3, activation='softmax'))

model.compile(loss=tf.keras.losses.sparse_categorical_crossentropy,
              optimizer=tf.keras.optimizers.SGD(lr=0.01, momentum=0.9, nesterov=True))

model.fit(x_train, y_train, epochs=20, batch_size=32)

loss_and_metrics = model.evaluate(x_test, y_test, batch_size=128)
print("loss and metrics: ", loss_and_metrics)

prediction = model.predict(x_test, batch_size=128)
prediction = prediction.argmax(1)

print("accuracy score: ", accuracy_score(y_test, prediction))
