import os
from time import time

import numpy as np
import scipy.io
import tensorflow as tf

from src import metrics_helper
from src.models import mlp

TRAIN_DATASET_PATH = '/home/mint/Scrivania/Tesi/Carotaggi/dataset carotaggi per il train/train'
TEST_DATASET_PATH = '/home/mint/Scrivania/Tesi/Carotaggi/dataset carotaggi per il test/datasetTEST'


def main(train, test):
    model_weigth = '../../model/model_%s_%s.h5' % (train, test)
    x_train, y_train = read_dataset(TRAIN_DATASET_PATH, train)
    x1, y1, x2, y2 = [], [], [], []
    for x, y in zip(x_train, y_train):
        if y[0] == 1:
            x1.append(x)
            y1.append(y)
        else:
            x2.append(x)
            y2.append(y)
    x1 = np.array(x1)
    x2 = np.array(x2)
    y1 = np.array(y1)
    y2 = np.array(y2)

    x_test, y_test = read_dataset(TEST_DATASET_PATH, test)

    model = mlp.version_2(32)
    # tensorboard = tf.keras.callbacks.TensorBoard(
    #     log_dir="../../logs/%s_%s_%s_%s" % ('mlp_prova2', train, test, str(time())))
    # check_point = tf.keras.callbacks.ModelCheckpoint(model_weigth, monitor='val_loss', verbose=0,
    #                                                  save_best_only=False,
    #                                                  save_weights_only=True, mode='auto', period=1)

    tf.keras.backend.set_value(model.optimizer.lr, .00001)
    print('Learning rate:', tf.keras.backend.get_value(model.optimizer.lr))
    model.fit(x1, y1, batch_size=10000, epochs=30)
    tf.keras.backend.set_value(model.optimizer.lr, .000015)
    print('Learning rate:', tf.keras.backend.get_value(model.optimizer.lr))
    model.fit(x2, y2, batch_size=10000, epochs=20)
    tf.keras.backend.set_value(model.optimizer.lr, .00001)
    print('Learning rate:', tf.keras.backend.get_value(model.optimizer.lr))
    model.fit(x_train, y_train, batch_size=10000, epochs=10)

    raw_predictions = model.predict(x_test)
    predictions = [x.argmax() for x in raw_predictions]
    y_test = [x.argmax() for x in y_test]

    return metrics_helper.compute_all_metrics(y_test, predictions)


def read_dataset(dataset_path, case):
    feature_path = os.path.join(dataset_path, 'dataset%s.mat' % case)
    label_path = os.path.join(dataset_path, 'truth%s.mat' % case)
    feature = scipy.io.loadmat(feature_path)['dataset']
    raw_label = scipy.io.loadmat(label_path)['truth']
    label = []
    for l in raw_label:
        if l[0] == 0:
            label.append([1, 0])
        else:
            label.append([0, 1])
    return feature, np.array(label)


if __name__ == '__main__':
    metrics = main('1', '1TEST')
    for k, v in metrics.items():
        print(k, v)
