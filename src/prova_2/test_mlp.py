import os

import matplotlib.pyplot as plt
import numpy as np
import scipy.misc

from src.models import mlp
from src.prova_2 import train_mlp

DATASET_PATH = '../../dataset/cervello'
IMAGES = train_mlp.IMAGES
LABELS = train_mlp.LABELS

MODEL_WEIGHT = train_mlp.MODEL_WEIGHT
BOX_EDGE_SIZE = train_mlp.BOX_EDGE_SIZE


def main():
    images_path = os.path.join(DATASET_PATH, IMAGES)
    labels_path = os.path.join(DATASET_PATH, LABELS)

    all_files_names = os.listdir(labels_path)

    model = mlp.version_2((BOX_EDGE_SIZE * 2 * BOX_EDGE_SIZE * 2))
    model.load_weights(train_mlp.MODEL_WEIGHT)

    for file_name in all_files_names[:2]:
        img_path = os.path.join(images_path, file_name)
        raw_img = scipy.misc.imread(img_path)

        label_path = os.path.join(labels_path, file_name)
        raw_label = scipy.misc.imread(label_path)

        imgs, labels = train_mlp.preprocess(raw_img, raw_label, BOX_EDGE_SIZE)
        imgs = np.array(imgs)
        labels = np.array(labels)
        predictions = model.predict(imgs)
        h_len = raw_img.shape[0] - 2 * BOX_EDGE_SIZE
        w_len = raw_img.shape[1] - 2 * BOX_EDGE_SIZE
        pred_img = [[0 for _ in range(w_len)] for _ in range(h_len)]
        for h in range(h_len):
            for w in range(w_len):
                index = (h * h_len) + w
                pred_img[h][w] = predictions[index].argmax(0) * 255

        f, axarr = plt.subplots(2, 3)
        axarr[0, 0].imshow(raw_label, cmap="Blues")
        axarr[0, 1].imshow(raw_img, cmap=plt.cm.bone)
        axarr[0, 1].imshow(pred_img, cmap="Blues", alpha=0.5)
        axarr[0, 2].imshow(raw_img, cmap=plt.cm.bone)
        axarr[1, 1].imshow(pred_img, cmap="Blues")
        plt.suptitle('Segmentation %s' % file_name)
        figManager = plt.get_current_fig_manager()
        figManager.full_screen_toggle()
        plt.show()


if __name__ == '__main__':
    main()
