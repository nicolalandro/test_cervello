import os
from time import time

import numpy as np
import scipy.misc
import tensorflow as tf

from src.models import mlp

DATASET_PATH = '../../dataset/cervello2'
IMAGES = 'images'
LABELS = 'labels'

MODEL_WEIGHT = "../../model/model_prova2.h5"
BATCH_SIZE = 1
EPOCHS = 22
BOX_EDGE_SIZE = 3


def main():
    images_path = os.path.join(DATASET_PATH, IMAGES)
    labels_path = os.path.join(DATASET_PATH, LABELS)

    all_files_names = os.listdir(labels_path)

    model = mlp.version_2(BOX_EDGE_SIZE * BOX_EDGE_SIZE * 4)
    tensorboard = tf.keras.callbacks.TensorBoard(log_dir="../../logs/%s_%s" % ('mlp_prova2', time()))
    check_point = tf.keras.callbacks.ModelCheckpoint(MODEL_WEIGHT, monitor='val_loss', verbose=0, save_best_only=False,
                                                     save_weights_only=True, mode='auto', period=1)
    for i in range(10):
        model.fit_generator(
            preprocess_generator(images_path, labels_path, all_files_names,
                                 BATCH_SIZE, BOX_EDGE_SIZE),
            epochs=EPOCHS, steps_per_epoch=5, callbacks=[tensorboard, check_point]
        )


def preprocess_generator(images_path, labels_path, all_files_names, batch_size, box_edge):
    for chunk_files_names in chunks(all_files_names, batch_size):
        chunk_imgs = []
        chunk_labels = []
        for file_name in chunk_files_names:
            img_path = os.path.join(images_path, file_name)
            raw_img = scipy.misc.imread(img_path)

            label_path = os.path.join(labels_path, file_name)
            raw_label = scipy.misc.imread(label_path)

            imgs, labels = preprocess(raw_img, raw_label, box_edge)

            chunk_imgs += imgs
            chunk_labels += labels
        yield np.array(chunk_imgs), np.array(chunk_labels)


def preprocess(raw_img, raw_label, box_edge):
    imgs = []
    labels = []
    for h in range(box_edge, raw_img.shape[0] - box_edge):
        for w in range(box_edge, raw_img.shape[1] - box_edge):
            imgs.append(window_stack(raw_img, h, w, box_edge))
            if raw_label[h][w] == 0:
                labels.append([1, 0])
            else:
                labels.append([0, 1])
    return imgs, labels


def window_stack(l, x, y, width):
    m = l[x - width:x + width, y - width:y + width]

    return [v.item() for line in m for v in line]


def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]


if __name__ == '__main__':
    main()
