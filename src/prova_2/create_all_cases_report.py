import csv
import os
from time import time

from src.prova_2 import train_with_cases

# CASES = [
#     ('1', '1TEST'),
#     ('1', '2TEST'),
#     ('1', '3TEST'),
#     ('1', '4TEST'),
#     ('2', '2TEST'),
#     ('2', '1TEST'),
#     ('2', '3TEST'),
#     ('2', '4TEST'),
#     ('3', '3TEST'),
#     ('3', '1TEST'),
#     ('3', '2TEST'),
#     ('3', '4TEST'),
#     ('4', '4TEST'),
#     ('4', '1TEST'),
#     ('4', '2TEST'),
#     ('4', '3TEST'),
#     ('123', '4TEST'),
#     ('124', '3TEST'),
#     ('134', '2TEST'),
#     ('234', '1TEST'),
#     ('2', '2TEST'),
#     ('1', '1TEST'),
#     ('3', '3TEST'),
#     ('4', '4TEST'),
#     ('123', '123TEST'),
#     ('124', '124TEST'),
#     ('134', '134TEST'),
#     ('234', '234TEST'),
#     ('1234', '1234TEST'),
#     ('ALL', 'ALL')
# ]

CASES = []
TRAIN_DATASET_PATH = '/home/mint/Scrivania/Tesi/Carotaggi/dataset carotaggi per il train/train'
train_paths = os.listdir(TRAIN_DATASET_PATH)
train_paths = list(filter(lambda x: 'dataset' in x, train_paths))
TEST_DATASET_PATH = '/home/mint/Scrivania/Tesi/Carotaggi/dataset carotaggi per il test/datasetTEST'
test_paths = os.listdir(TEST_DATASET_PATH)
test_paths = list(filter(lambda x: 'dataset' in x, test_paths))

for train_p in train_paths:
    for test_p in test_paths:
        CASES.append((train_p[7:-4], test_p[7:-4]))

METRICS = ['accuracy', 'sensitivity', 'specifity', 'precision', 'recall', 'f_measure', 'geometric mean', 'jaccard']


def main():
    csv_output_name = 'analisi_%s_.csv' % str(time())
    append_csv_line(csv_output_name, ['case_name'] + METRICS)
    skip = True
    for train, test in CASES:
        skip_point_train = '24_1'
        skip_point_test = '28_1'
        if train == skip_point_train and test == skip_point_test:
            skip = False
        if skip:
            print('skipp:', train, test)
            continue
        print('start case:', train, test)
        try:
            evaluation = train_with_cases.main(train, test)
            line = ['%s_%s' % (train, test)] + [evaluation[m] for m in METRICS]
            append_csv_line(csv_output_name, line)
        except:
            append_csv_line('error.ls', ['%s_%s' % (train, test)])
        print('end case:', train, test)


def append_csv_line(file_name, line):
    with open(file_name, 'a') as f:
        csv_writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(line)


if __name__ == '__main__':
    main()
