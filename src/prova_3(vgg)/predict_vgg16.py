import os

import tensorflow as tf

TUMOR_PATH = '/home/mint/PycharmProjects/train_cervello/dataset/tumor_classification/tumor'
NON_TUMOR_PATH = '/home/mint/PycharmProjects/train_cervello/dataset/tumor_classification/non_tumor'

tumor_image = os.path.join(TUMOR_PATH, 'caso_001_122.jpg')

model = tf.keras.applications.vgg16.VGG16(weights='imagenet', include_top=True)
output = tf.keras.layers.Flatten(name='flatten2')(model.output)
predictions = tf.keras.layers.Dense(units=2, activation=tf.keras.activations.softmax)(output)
new_model = tf.keras.Model(model.input, predictions)
new_model.summary()


image = tf.keras.preprocessing.image.load_img(tumor_image, target_size=(224, 224))
image = tf.keras.preprocessing.image.img_to_array(image)
image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
image = tf.keras.applications.vgg16.preprocess_input(image)

yhat = new_model.predict(image)
print(len(yhat[0]))
