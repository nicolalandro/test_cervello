# Tumori da mammografie
* [notebook](https://localhost:8888/notebooks/Mammografie.ipynb)
* Size 4440 x 3972
* valori del singolo pixel 0...~60000
* valori del singolo pixel della lable 0..255 (non solo gli estremi anche altri inmezzo)

# Tumori da Tac al cervello
* mlp su singolo pixel -> risultati più basi
* mlp su intorno come la SVM -> risultati più basi
* Deeplearning
  * Segnet (da provare)
  * FCN (in testing)
  * Unet -> non da buoni risultati
* Può darsi che sbagliamo la loss function? Se tentassimo di minimzzare Jaccard?
  * 1 - ( (intersezione + 0,01)/(true + pred - intersezione + 0,01) )
  * è compreso tra 0 e 1

# Tool GUI
* Il tool è passato dalla codebase spike a quella effettiva
* mantenendo però tutto il debito tecnico sulla parte di js e html
* Ci sono delle feature non ancora implementate
  * (WIP) creare il concetto di progetto con i suoi nodi dedicati, grafi dedicati etc.
  * gestire il plugin del deep learning come progetto, in questo modo può stare come code base esterna e posso creare uno script che lo genera facilmente
    * (NEW) gestire la scrittura di nuovi tipi di layer
    * (NEW) gestire la scrittura di nuove loss function
  * il grafo deve eseguire asincrono e una websocket avverte quando è terminata l'esecuzione o quando c'è un nuovo output, perchè un train può durare giorni e non si può aspettare una post per giorni, il server va in timeout
  * blocco FOR & WHILE
  * blocco per lanciare un processo (e ucciderlo), così potrei scegliere tra mlflow & tensorboard
  * Test front-end
  * Video dei test di accettazione come documentazione?

# Tesi
* [github](https://gitlab.com/nicolalandro/tesi_magistrale)
* Appena iniziato il capitolo dei prerequisiti