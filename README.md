# Test cervello
* Create dataset image:
```bash
cd dataset
mkdir cervello
cd cervello
mkdir images
mkdir labels
cd ../../data_code
python create_segmentation_dataset.py
# with crop, create_segmentation_dataset_with_crop.py
```

* Jupyter
```bash
jupyter notebook
```


# Prova 1
* Addestro una NN
* input pixel
* output tumore/non tumore

## Dataset
* ogni pixel di ogni immagine
```bash
cd data_code/prova_1
python create_pixel_dataset.py
 ```
* ogni pixel in un intorno del tumore
```bash
cd data_code/prova_1
python create_detection_segmentation_pixel_dataset.py
```
## Train
* Extra tree
```bash
cd src/prova_1
python train_extra_trees.py
python test_image_extra_trees.py
```
* MLP
```bash
cd src/prova_1
python train_mlp.py
python test_image_mlp.py
```    
# Prova 2
* "Create dataset image" (come spiegato sopra)
* Train
```bash
cd src/prova_2
python train_mlp.py
```
* test
```bash
cd src/prova_2
python test_mlp
```